# escape=`
FROM mcr.microsoft.com/windows/servercore:1809-amd64 as base

FROM base as install

RUN powershell Invoke-WebRequest  'https://github.com/msys2/msys2-installer/releases/download/nightly-x86_64/msys2-base-x86_64-latest.sfx.exe' -Outfile 'C:\msys2.exe'

# Extract to C:\msys64
RUN msys2.exe -y -oC:\\


FROM base

RUN  setx /M path "C:\msys64\usr\local\bin;C:\msys64\usr\bin;C:\msys64\bin;C:\msys64\usr\bin\site_perl;C:\msys64\usr\bin\vendor_perl;C:\msys64\usr\bin\core_perl;%PATH%"

COPY --from=install C:\msys64 C:\msys64

# Run for the first time
RUN C:\msys64\usr\bin\bash -lc " "

#  Preserve the current working directory
RUN  setx /M CHERE_INVOKING yes

# Start a 64 bit Mingw environment
RUN  setx /M  MSYSTEM UCRT64



# Update MSYS2
RUN C:/msys64/usr/bin/bash.exe -l -c 'pacman --noconfirm -Syuu'
# I don't understand this but it's direct from the source
RUN C:/msys64/usr/bin/bash.exe -l -c 'pacman --noconfirm -Syuu'

# Install packages
RUN C:/msys64/usr/bin/bash.exe -l -c 'pacman --noconfirm -Sy bash zip gzip tar'

ENTRYPOINT [ "C:/msys64/usr/bin/bash.exe", "-l" , "-c", "$@" ]
