# Msys2 windows docker image

I was stuck not being able to use some common bash utilities and I had to do do this.

The infra for doing this is extremely slow, and having a job even attempt to start to build this, even if cached, is slow as f.  So pulling it out as a separate project  seemed pretty useful.

# Use

_Remember this is in a windows env_

docker pull registry.gitlab.com/jkushmaul/msys2

* Use it directly:

```
docker run --rm -it -v ${pwd}:${pwd} registry.gitlab.com/jkushmaul/msys2 bash -c my_bash_script.sh
```

* Extract msys2 out  _say you really need it in the host env_

```
docker run --rm -it -v ${pwd}:${pwd} registry.gitlab.com/jkushmaul/msys2 cp -R /c/msys64 ./msys64

$Env:Path += "${pwd}\msys64\usr\local\bin;${pwd}\msys64\usr\bin;${pwd}\msys64\bin;${pwd}\msys64\usr\bin\site_perl;${pwd}\msys64\usr\bin\vendor_perl;${pwd}\msys64\usr\bin\core_perl"


#  Preserve the current working directory
$Env:CHERE_INVOKING="yes"

# Start a 64 bit Mingw environment
$Env:MSYSTEM="UCRT64"

.\msys64\usr\bin\bash.exe -l -c my_bash_script.sh

```
